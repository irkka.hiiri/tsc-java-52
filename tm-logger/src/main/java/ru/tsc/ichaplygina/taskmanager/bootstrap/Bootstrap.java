package ru.tsc.ichaplygina.taskmanager.bootstrap;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.service.IReceiverService;
import ru.tsc.ichaplygina.taskmanager.listener.LoggerListener;
import ru.tsc.ichaplygina.taskmanager.service.ReceiverService;

public class Bootstrap {

    @SneakyThrows
    public void init() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());

    }

}
