package ru.tsc.ichaplygina.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.endpoint.*;
import ru.tsc.ichaplygina.taskmanager.marker.SoapCategory;

import javax.xml.ws.WebServiceException;

public class TaskEndpointTest {

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private Session session;

    @Before
    public void initTest() {
        session = sessionEndpoint.openSession("root", "toor");
        projectEndpoint.createProject(session, "test project 1", "");
        projectEndpoint.createProject(session, "test project 2", "");
        taskEndpoint.createTask(session, "task 1", "desc 1");
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "test project 1").getId();
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        taskEndpoint.addTaskToProject(session, projectId, taskId);
    }

    @Test
    @Category(SoapCategory.class)
    public void testAddTaskToProject() {
        taskEndpoint.createTask(session, "task 2", "");
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "test project 2").getId();
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 2").getId();
        taskEndpoint.addTaskToProject(session, projectId, taskId);
        Assert.assertEquals(1, taskEndpoint.getTaskListByProject(session, projectId, "").size());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testAddTaskToProjectUnknownProject() {
        taskEndpoint.createTask(session, "task 2", "");
        @NotNull final String projectId = "???";
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 2").getId();
        taskEndpoint.addTaskToProject(session, projectId, taskId);
    }

    @Test
    @Category(SoapCategory.class)
    public void testClearTasks() {
        taskEndpoint.clearTasks(session);
        Assert.assertEquals(0, taskEndpoint.getTaskList(session, "").size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteTaskById() {
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        taskEndpoint.completeTaskById(session, taskId);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteTaskByIdBadId() {
        @NotNull final String taskId = "???";
        taskEndpoint.completeTaskById(session, taskId);
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteTaskByIndex() {
        taskEndpoint.completeTaskByIndex(session, 0);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteTaskByIndexBadIndex() {
        taskEndpoint.completeTaskByIndex(session, 1);
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteTaskByName() {
        taskEndpoint.completeTaskByName(session, "task 1");
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteTaskByNameBadName() {
        taskEndpoint.completeTaskByName(session, "Bad Name");
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateTask() {
        taskEndpoint.createTask(session, "task 2", "desc 1");
        Assert.assertEquals(2, taskEndpoint.getTaskList(session, "").size());
        taskEndpoint.removeTaskByName(session, "task 2");
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskList() {
        Assert.assertEquals(1, taskEndpoint.getTaskList(session, "").size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskListByProject() {
        @NotNull final String projectIdWithTask = projectEndpoint.findProjectByName(session, "test project 1").getId();
        @NotNull final String emptyProjectId = projectEndpoint.findProjectByName(session, "test project 2").getId();
        Assert.assertEquals(1, taskEndpoint.getTaskListByProject(session, projectIdWithTask, "").size());
        Assert.assertEquals(0, taskEndpoint.getTaskListByProject(session, emptyProjectId, "").size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindTaskById() {
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        Assert.assertNotNull(taskEndpoint.findTaskById(session, taskId));
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindTaskByIdBadId() {
        @NotNull final String taskId = "...";
        Assert.assertNull(taskEndpoint.findTaskById(session, taskId));
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindTaskByIndex() {
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(session, 0));
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testFindTaskByIndexBadIndex() {
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(session, 1));
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindTaskByName() {
        Assert.assertNotNull(taskEndpoint.findTaskByName(session, "task 1"));
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindTaskByNameBadName() {
        Assert.assertNull(taskEndpoint.findTaskByName(session, "unknown name"));
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskById() {
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        taskEndpoint.removeTaskById(session, taskId);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveTaskByIdBadId() {
        @NotNull final String taskId = "???";
        taskEndpoint.removeTaskById(session, taskId);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskByIndex() {
        taskEndpoint.removeTaskByIndex(session, 0);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveTaskByIndexBadIndex() {
        taskEndpoint.removeTaskByIndex(session, 1);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskByName() {
        taskEndpoint.removeTaskByName(session, "task 1");
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveTaskByNameBadName() {
        taskEndpoint.removeTaskByName(session, "Bad Name");
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskFromProject() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "test project 1").getId();
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        taskEndpoint.removeTaskFromProject(session, projectId, taskId);
        Assert.assertEquals(0, taskEndpoint.getTaskListByProject(session, projectId, "").size());
        taskEndpoint.addTaskToProject(session, projectId, taskId);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveTaskFromProjectWrongProject() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "test project 2").getId();
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        taskEndpoint.removeTaskFromProject(session, taskId, projectId);
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartTaskById() {
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        taskEndpoint.startTaskById(session, taskId);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartTaskByIdBadId() {
        @NotNull final String taskId = "???";
        taskEndpoint.startTaskById(session, taskId);
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartTaskByIndex() {
        taskEndpoint.startTaskByIndex(session, 0);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartTaskByIndexBadIndex() {
        taskEndpoint.startTaskByIndex(session, 1);
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartTaskByName() {
        taskEndpoint.startTaskByName(session, "task 1");
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartTaskByNameBadName() {
        taskEndpoint.startTaskByName(session, "Bad Name");
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateTaskById() {
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        taskEndpoint.updateTaskById(session, taskId, "task 1", "new description");
        @NotNull final Task task = taskEndpoint.findTaskByName(session, "task 1");
        Assert.assertEquals("new description", task.getDescription());

    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateTaskByIdBadId() {
        @NotNull final String taskId = "...";
        taskEndpoint.updateTaskById(session, taskId, "task 1", "new description");
        taskEndpoint.findTaskByName(session, "task 1");

    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateTaskByIndex() {
        taskEndpoint.updateTaskByIndex(session, 0, "task 1", "new description");
        @NotNull final Task task = taskEndpoint.findTaskByName(session, "task 1");
        Assert.assertEquals("new description", task.getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateTaskByIndexBadIndex() {
        taskEndpoint.updateTaskByIndex(session, 1, "task 1", "new description");
    }

    @After
    public void closeTest() {
        projectEndpoint.removeProjectByName(session, "test project 1");
        projectEndpoint.removeProjectByName(session, "test project 2");
        if (taskEndpoint.findTaskByName(session, "task 2") != null) taskEndpoint.removeTaskByName(session, "task 2");
        adminEndpoint.saveBackup(session);
        sessionEndpoint.closeSession(session);
    }

}
