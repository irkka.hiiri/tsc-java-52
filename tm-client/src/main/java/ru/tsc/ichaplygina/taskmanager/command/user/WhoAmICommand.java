package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.endpoint.Session;

public final class WhoAmICommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "whoami";

    @NotNull
    public static final String DESCRIPTION = "print current user login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final Session currentSession = getSession();
        if (currentSession == null) System.out.println("User not logged in");
        else System.out.println(getAdminEndpoint().findUserById(currentSession, currentSession.getUser().getId()).getLogin());
    }

}
