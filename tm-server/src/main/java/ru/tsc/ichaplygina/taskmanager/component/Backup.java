package ru.tsc.ichaplygina.taskmanager.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.service.IDomainService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private final IDomainService domainService;
    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    @NotNull
    private final IPropertyService propertyService;

    public Backup(@NotNull final IDomainService domainService, @NotNull final IPropertyService propertyService) {
        this.domainService = domainService;
        this.propertyService = propertyService;
    }

    public void init() {
        load();
        start();
    }

    private void load() {
        domainService.loadBackup();
    }

    @Override
    @SneakyThrows
    public void run() {
        save();
    }

    private void save() {
        domainService.saveBackup();
    }

    public void start() {
        executorService.scheduleWithFixedDelay(this, 0, propertyService.getAutosaveFrequency(), TimeUnit.MILLISECONDS);
    }

}
