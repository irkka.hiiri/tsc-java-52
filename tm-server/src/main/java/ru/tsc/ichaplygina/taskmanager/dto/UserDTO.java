package ru.tsc.ichaplygina.taskmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.listener.EntityListener;

import javax.persistence.*;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.DELIMITER;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_user")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractModelDTO {

    @NotNull
    @Column
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Column
    private boolean locked;

    @NotNull
    @Column
    private String login;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(name = "password")
    private String passwordHash;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role;

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash, @NotNull final String email, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.role = role;
        this.locked = false;
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash, @NotNull final String email, @Nullable final String firstName,
                   @Nullable final String middleName, @Nullable final String lastName, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.role = role;
        this.locked = false;
    }

    public final String getFullName() {
        return (isEmptyString(firstName) ? "" : firstName + " ") +
                (isEmptyString(middleName) ? "" : middleName + " ") +
                (isEmptyString(lastName) ? "" : lastName);
    }

    @Override
    public final String toString() {
        return getId() + DELIMITER + login + DELIMITER + email + DELIMITER + "Role: " + role +
                (isEmptyString(getFullName()) ? "" : DELIMITER + "Full Name: " + getFullName());
    }

}


