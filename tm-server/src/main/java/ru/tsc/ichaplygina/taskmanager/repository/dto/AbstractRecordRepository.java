package ru.tsc.ichaplygina.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.IAbstractRecordRepository;
import ru.tsc.ichaplygina.taskmanager.dto.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractRecordRepository<E extends AbstractModelDTO> implements IAbstractRecordRepository<E> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRecordRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull E entity) {
        entityManager.persist(entity);
    }

    @Override
    public abstract void clear();

    @NotNull
    @Override
    public abstract List<E> findAll();

    @Nullable
    @Override
    public abstract E findById(@NotNull String id);

    @Override
    public abstract long getSize();

    @Override
    public abstract void removeById(@NotNull final String id);

    @Override
    public void update(@NotNull E entity) {
        entityManager.merge(entity);
    }

}
