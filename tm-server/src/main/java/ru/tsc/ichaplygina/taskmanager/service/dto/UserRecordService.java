package ru.tsc.ichaplygina.taskmanager.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.IUserRecordRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IUserRecordService;
import ru.tsc.ichaplygina.taskmanager.dto.UserDTO;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.empty.EmailEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithEmailException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithLoginException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.repository.dto.UserRecordRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public final class UserRecordService extends AbstractRecordService<UserDTO> implements IUserRecordService {

    @NotNull
    private final IPropertyService propertyService;

    public UserRecordService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public final void add(@NotNull final String login, @NotNull final String password, @NotNull final String email, @NotNull final Role role,
                          @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        @NotNull final UserDTO user = new UserDTO(login, Objects.requireNonNull(salt(password, propertyService)), email, firstName, middleName, lastName, role);
        add(user);
    }

    @Override
    @SneakyThrows
    public final void add(@NotNull final UserDTO user) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            entityManager.getTransaction().begin();
            if (!isEmptyString(repository.findIdByLogin(user.getLogin())))
                throw new UserExistsWithLoginException(user.getLogin());
            if (!isEmptyString(repository.findIdByEmail(user.getEmail())))
                throw new UserExistsWithEmailException(user.getEmail());
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@Nullable List<UserDTO> users) {
        if (users == null) return;
        for (final UserDTO user : users) add(user);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public final UserDTO findByLogin(@NotNull final String login) {
        return Optional.ofNullable(findUserByLogin(login)).orElseThrow(UserNotFoundException::new);
    }

    @Nullable
    @Override
    public final UserDTO findByLoginForAuthorization(@NotNull final String login) {
        return Optional.ofNullable(findUserByLogin(login)).orElseThrow(IncorrectCredentialsException::new);
    }

    @Nullable
    @SneakyThrows
    private String findIdByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            return repository.findIdByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    private UserDTO findUserByLogin(@NotNull final String login) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            return repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return (getSize() == 0);
    }

    @Override
    public final boolean isPrivilegedUser(@NotNull final String userId) {
        @NotNull final UserDTO user = Optional.ofNullable(findById(userId)).orElseThrow(UserNotFoundException::new);
        return user.getRole().equals(Role.ADMIN);
    }

    @Override
    @SneakyThrows
    public final boolean lockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            @NotNull final UserDTO user = Optional.ofNullable(repository.findById(id)).orElseThrow(UserNotFoundException::new);
            if (user.isLocked()) return false;
            user.setLocked(true);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return true;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean lockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            @NotNull final UserDTO user = Optional.ofNullable(repository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            if (user.isLocked()) return false;
            user.setLocked(true);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return true;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            @NotNull final UserDTO user = Optional.ofNullable(repository.findById(id)).orElseThrow(UserNotFoundException::new);
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public final UserDTO removeByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            @NotNull final UserDTO user = Optional.ofNullable(repository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            entityManager.getTransaction().begin();
            repository.removeByLogin(login);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final void setPassword(@NotNull final String login, @NotNull final String password) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            @NotNull final UserDTO user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
            user.setPasswordHash(salt(password, propertyService));
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final void setRole(@NotNull final String login, @NotNull final Role role) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            @NotNull final UserDTO user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
            user.setRole(role);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean unlockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            @NotNull final UserDTO user = Optional.ofNullable(repository.findById(id)).orElseThrow(UserNotFoundException::new);
            if (!user.isLocked()) return false;
            user.setLocked(false);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return true;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean unlockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            @NotNull final UserDTO user = Optional.ofNullable(repository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            if (!user.isLocked()) return false;
            user.setLocked(false);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return true;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public final UserDTO updateById(@NotNull final String id, @NotNull final String login, @NotNull final String password, @NotNull final String email,
                                    @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(entityManager);
            @Nullable final UserDTO userFoundWithThisLogin = repository.findByLogin(login);
            @Nullable final UserDTO userFoundWithThisEmail = repository.findByEmail(email);
            if (userFoundWithThisLogin != null && !id.equals(userFoundWithThisLogin.getId()))
                throw new UserExistsWithLoginException(login);
            if (userFoundWithThisEmail != null && !id.equals(userFoundWithThisEmail.getId()))
                throw new UserExistsWithEmailException(email);
            @NotNull final UserDTO user = Optional.ofNullable(findById(id)).orElseThrow(UserNotFoundException::new);
            user.setLogin(login);
            user.setPasswordHash(password);
            user.setEmail(email);
            user.setRole(role);
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setLastName(lastName);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public final UserDTO updateByLogin(@NotNull final String login, @NotNull final String password, @NotNull final String email,
                                       @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final String id = Optional.ofNullable(findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
        return updateById(id, login, password, email, role, firstName, middleName, lastName);
    }

}
