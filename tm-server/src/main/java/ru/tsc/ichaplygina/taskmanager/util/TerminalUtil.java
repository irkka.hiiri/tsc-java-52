package ru.tsc.ichaplygina.taskmanager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.enumerated.Sort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.NEW_LINE;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.SORT_INPUT;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@UtilityClass
public final class TerminalUtil {

    @NotNull
    private final static Scanner SCANNER = new Scanner(System.in);

    public static void printLinesWithEmptyLine(@Nullable final Object... lines) {
        if (lines == null) return;
        System.out.println();
        for (@Nullable final Object line : lines) System.out.println(line);
        System.out.println();
    }

    public static <T> void printList(@NotNull final List<T> list) {
        System.out.println();
        for (@NotNull final Object item : list) {
            System.out.println(item);
        }
        System.out.println();
    }

    public static <T> void printListWithIndexes(@NotNull final List<T> list) {
        System.out.println();
        int index = 1;
        for (@NotNull final Object item : list) {
            System.out.println(index + ". " + item);
            index++;
        }
        System.out.println();
    }

    public static Comparator readComparator() {
        @NotNull final String sort = readLine(SORT_INPUT + " " + Arrays.toString(Sort.values()) + NEW_LINE);
        if (isEmptyString(sort)) return Sort.CREATED.getComparator();
        try {
            return Sort.valueOf(sort.toUpperCase()).getComparator();
        } catch (@NotNull final IllegalArgumentException e) {
            return Sort.CREATED.getComparator();
        }
    }

    @NotNull
    public static String readLine() {
        return SCANNER.nextLine().trim();
    }

    @NotNull
    public static String readLine(@NotNull final String output) {
        System.out.print(output);
        return readLine();
    }

    public static int readNumber() {
        try {
            return Integer.parseInt(readLine());
        } catch (@NotNull final NumberFormatException e) {
            return -1;
        }
    }

    public static int readNumber(@NotNull final String output) {
        System.out.print(output);
        return readNumber();
    }

    @NotNull
    public static Role readRole(@NotNull final String output) {
        @NotNull final String role = readLine(output + " " + Arrays.toString(Role.values()) + NEW_LINE);
        if (isEmptyString(role)) return Role.USER;
        try {
            return Role.valueOf(role.toUpperCase());
        } catch (@NotNull final IllegalArgumentException e) {
            return Role.USER;
        }
    }

}
