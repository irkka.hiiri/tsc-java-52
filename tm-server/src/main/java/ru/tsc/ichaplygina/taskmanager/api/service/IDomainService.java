package ru.tsc.ichaplygina.taskmanager.api.service;

import lombok.SneakyThrows;

public interface IDomainService {


    @SneakyThrows
    void loadBackup();

    @SneakyThrows
    void loadBase64();

    @SneakyThrows
    void loadBinary();

    @SneakyThrows
    void loadJsonFasterXML();

    @SneakyThrows
    void loadJsonJaxb();

    @SneakyThrows
    void loadXMLFasterXML();

    @SneakyThrows
    void loadXMLJaxb();

    @SneakyThrows
    void loadYAMLFasterXML();

    @SneakyThrows
    void saveBackup();

    @SneakyThrows
    void saveBase64();

    @SneakyThrows
    void saveBinary();

    @SneakyThrows
    void saveJsonFasterXML();

    @SneakyThrows
    void saveJsonJaxb();

    @SneakyThrows
    void saveXMLFasterXML();

    @SneakyThrows
    void saveXMLJaxb();

    @SneakyThrows
    void saveYAMLFasterXML();
}
